/**
 * Created by Kenneth on 10/8/2016.
 */

(function(){
    angular
        .module("singboapp")
        .controller("DefaultAppCtrl", DefaultAppCtrl)
        .controller("CustomAppCtrl", CustomAppCtrl)
        .controller("CalendarAppCtrl", CalendarAppCtrl)
        .controller("TodoAppCtrl", TodoAppCtrl)
        .controller("ReciepeAppCtrl", ReciepeAppCtrl)
        .controller("LoginCtrl", LoginCtrl)
        .controller("LogoutCtrl", LogoutCtrl)
        .controller("RegisterCtrl", RegisterCtrl)
        .controller("ProfileCtrl", ProfileCtrl)
        .controller("ResetPasswordCtrl", ResetPasswordCtrl)
        .controller("ClockAppCtrl", ClockAppCtrl);

    function LoginCtrl($http, $q, $sanitize, $state, dbService, AuthService, Flash){
        var vm = this;

        vm.login = function () {
            AuthService.login(vm.user)
                .then(function () {
                    if(AuthService.isLoggedIn()){
                        vm.emailAddress = "";
                        vm.password = "";
                        $state.go("defaultApp");
                    }else{
                        Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                        $state.go("SignIn");
                    }
                }).catch(function () {
                    console.error("Error logging on !");
                });
        };
    }

    LoginCtrl.$inject  = ["$http", "$q", "$sanitize", "$state", "dbService", "AuthService","Flash"];

    function LogoutCtrl($http, $q, $sanitize, $state, dbService, AuthService, Flash){
        var vm = this;

        vm.logout = function () {
            AuthService.logout()
                .then(function () {
                    $state.go("SignIn");
                }).catch(function () {
                    console.error("Error logging on !");
                });
        };
    }

    LogoutCtrl.$inject  = ["$http", "$q", "$sanitize", "$state", "dbService", "AuthService","Flash"];


    function ProfileCtrl($http, $q, $sanitize, $state, dbService){
        var vm = this;
        vm.sociallogins = [];
        dbService.getLocalProfile().then(function(result){
            vm.localProfile = result.data;
        });

        dbService.getAllSocialLoginsProfile().then(function(result){
            vm.sociallogins = result.data;
        });

    }

    ProfileCtrl.$inject  = ["$http", "$q", "$sanitize", "$state", "dbService"];

    function RegisterCtrl($http, $q, $sanitize, $state, dbService, AuthService, Flash){
        var vm = this;
        vm.emailAddress = "";
        vm.password = "";
        vm.confirmpassword = "";
        
        vm.register = function () {
            AuthService.register($sanitize(vm.emailAddress), $sanitize(vm.password))
                .then(function () {
                    vm.disabled = false;

                    vm.emailAddress = "";
                    vm.password = "";
                    vm.confirmpassword = "";
                    Flash.clear();
                    Flash.create('success', "Successfully sign up with us, Please proceed to login", 0, {class: 'custom-class', id: 'custom-id'}, true);
                    $state.go("SignIn");
                }).catch(function () {
                    console.error("registration having issues");
                });
        };

    }

    RegisterCtrl.$inject  = ["$http", "$q", "$sanitize", "$state", "dbService", "AuthService", "Flash"];


    function ResetPasswordCtrl($http, $q, $sanitize,dbService){
        var vm = this;
        vm.emailAddress = "";

        vm.resetPassword = function () {
            console.info($sanitize(vm.emailAddress));

        };
    }

    ResetPasswordCtrl.$inject  = ["$http", "$q", "$sanitize", "dbService"];


    function CalendarAppCtrl($http, $q, dbService){

    }

    CalendarAppCtrl.$inject  = ["$http", "$q", "dbService"];


    function TodoAppCtrl($http, $q, dbService){

    }

    TodoAppCtrl.$inject  = ["$http", "$q", "dbService"];


    function ReciepeAppCtrl($http, $q, dbService){

    }

    ReciepeAppCtrl.$inject  = ["$http", "$q", "dbService"];


    function ClockAppCtrl($http, $q, dbService){

    }

    ClockAppCtrl.$inject  = ["$http", "$q", "dbService"];


    function DefaultAppCtrl($q, dbService, AuthService){
        var vm = this;
        vm.results = [];
        vm.pageIndex = 0;
        const recordPerPage = 3;
        vm.totalRecords = 0;
        vm.totalPages = 0;
        vm.currentPage = 1;

        AuthService.getUserStatus(function(result){
            // hack for social logins ...
            // don't really understand why angular js didn't move to the next line of codes.
            vm.isUserLogon = result;
        });
        vm.isUserLogon = AuthService.isLoggedIn();

        var defer = $q.defer();
        vm.err = null;
        if(vm.isUserLogon){
            dbService.getTotalDefaultApps();

            dbService.defaultAppPaging(vm.pageIndex, recordPerPage)
                .then(function (results) {
                    vm.results = results.data;
                })
                .catch(function (err) {
                    console.error(err);
                });

            dbService.getTotalDefaultApps()
                .then(function (results) {
                    vm.totalRecords = results.data;
                    vm.totalPages = Math.ceil(vm.totalRecords/recordPerPage);
                })
                .catch(function (err) {
                    console.error(err);
                });


            vm.next = function () {
                vm.currentPage = vm.currentPage + 1;
                vm.pageIndex = vm.pageIndex + recordPerPage;
                dbService.defaultAppPaging(vm.pageIndex, recordPerPage)
                    .then(function (results) {
                        vm.results = results.data;
                    })
                    .catch(function (err) {
                        console.error(err);
                    });
            };

            vm.previous = function () {
                vm.currentPage = vm.currentPage - 1;
                vm.pageIndex = vm.pageIndex - recordPerPage;
                dbService.defaultAppPaging(vm.pageIndex, recordPerPage)
                    .then(function (results) {
                        vm.results = results.data;
                    })
                    .catch(function (err) {
                        console.error(err);
                    });
            };
        }
    }

    DefaultAppCtrl.$inject  = ["$q", "dbService", "AuthService"];

    function CustomAppCtrl($q, dbService, AuthService){
        var vm = this;
        vm.results = [];
        vm.pageIndex = 0;
        const recordPerPage = 3;
        vm.totalRecords = 0;
        vm.totalPages = 0;
        vm.currentPage = 1;
        vm.isUserLogon = AuthService.isLoggedIn();

        var defer = $q.defer();
        vm.err = null;

        if(vm.isUserLogon) {
            dbService.getTotalCustomApps();


            dbService.customAppPaging(vm.pageIndex, recordPerPage)
                .then(function (results) {
                    vm.results = results.data;
                })
                .catch(function (err) {
                    console.error(err);
                });

            dbService.getTotalCustomApps()
                .then(function (results) {
                    vm.totalRecords = results.data;
                    vm.totalPages = Math.ceil(vm.totalRecords / recordPerPage);
                    console.info(vm.totalPages);
                })
                .catch(function (err) {
                    console.error(err);
                });
        }

        vm.next = function () {
            console.info("Why bug?");
            vm.currentPage = vm.currentPage + 1;
            vm.pageIndex = vm.pageIndex + recordPerPage;
            dbService.customAppPaging(vm.pageIndex, recordPerPage)
                .then(function (results) {
                    vm.results = results.data;
                })
                .catch(function (err) {
                    console.error(err);
                });
        };

        vm.previous = function () {
            vm.currentPage = vm.currentPage - 1;
            vm.pageIndex = vm.pageIndex - recordPerPage;
            dbService.customAppPaging(vm.pageIndex, recordPerPage)
                .then(function (results) {
                    vm.results = results.data;
                })
                .catch(function (err) {
                    console.error(err);
                });
        };

    }

    CustomAppCtrl.$inject  = ["$q", "dbService", "AuthService"];

})();