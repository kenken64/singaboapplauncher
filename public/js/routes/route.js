/**
 * Created by Kenneth on 10/8/2016.
 */
(function () {

    angular
        .module("singboapp")
        .config(SingboLaunchAppConfig);

    function SingboLaunchAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("defaultApp", {
                url: "/default-app",
                views: {
                    "nav": {
                        templateUrl: "views/navbar.html"
                    },
                    "content": {
                        templateUrl: "views/defaultapp.html"
                    }
                },
                controller: "DefaultAppCtrl as dftCtrl"
            })
            .state("SignIn", {
                url: "/signIn",
                views: {
                    "nav": {
                        templateUrl: "views/navbar.html"
                    },
                    "content": {
                        templateUrl: "views/login.html"
                    }
                },
                controller: "LoginCtrl as ctrl"
            })
            .state("SignUp", {
                url: "/signUp",
                views: {
                    "nav": {
                        templateUrl: "views/navbar.html"
                    },
                    "content": {
                        templateUrl: "views/register.html"
                    }
                },
                controller: "RegisterCtrl as ctrl"
            })
            .state("resetPassword", {
                url: "/resetPassword",
                views: {
                    "nav": {
                        templateUrl: "views/navbar.html"
                    },
                    "content": {
                        templateUrl: "views/resetpassword.html"
                    }
                },
                controller: "ResetPasswordCtrl as ctrl"
            })
            .state("MyAccount", {
                url: "/MyAccount",
                views: {
                    "nav": {
                        templateUrl: "views/navbar.html"
                    },
                    "content": {
                        templateUrl: "views/profile.html"
                    }
                },
                controller: "MyAccountCtrl as ctrl"
            })
            .state("SystemInfo", {
                url: "/SystemInfo",
                views: {
                    "nav": {
                        templateUrl: "views/navbar.html"
                    },
                    "content": {
                        templateUrl: "views/sysinfo.html"
                    }
                },
                controller: "MyAccountCtrl as ctrl"
            })
            .state("calendarApp", {
                url: "/calendar",
                templateUrl: 'views/calendar.html',
                controller: "CalendarAppCtrl as ctrl"
            })
            .state("clockApp", {
                url: "/clock-app",
                templateUrl: "views/clock.html",
                controller: "ClockAppCtrl as ctrl"
            })
            .state("todoApp", {
                url: "/todo-app",
                templateUrl: "views/todo.html",
                controller: "TodoAppCtrl as ctrl"
            })
            .state("reciepeApp", {
                url: "/reciepe-app",
                templateUrl: "views/reciepe.html",
                controller: "ReciepeAppCtrl as ctrl"
            })
            .state("customApp", {
                url: "/custom-app",
                views: {
                    "nav": {
                        templateUrl: "views/navbar.html"
                    },
                    "content": {
                        templateUrl: "views/customapp.html"
                    }
                },
                controller: "CustomAppCtrl as customCtrl"
            });

        $urlRouterProvider.otherwise("/default-app");
    }

    SingboLaunchAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

})();