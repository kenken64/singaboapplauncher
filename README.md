Day 31 - User authentication, Social logics and Permission


```
#!sql

CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `encrypted_password` varchar(45) NOT NULL,
  `reset_password_token` varchar(45) DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT NULL,
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(100) DEFAULT NULL,
  `last_sign_in_ip` varchar(100) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `is_deleted` enum('Y','N') DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `index_admin_users_on_email` (`email`),
  UNIQUE KEY `index_admin_users_on_reset_password_token` (`reset_password_token`)
) 
```


```
#!sql


[CREATE TABLE `authentication_provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `providerId` varchar(128) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `providerType` varchar(100) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `displayName` varchar(200) DEFAULT NULL,
  `profile_photo` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `FK_AUTH_USER_idx` (`userId`),
  CONSTRAINT `FK_AUTH_PROVIDER_USER` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) 

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `encrypted_password` varchar(128) DEFAULT NULL,
  `reset_password_token` varchar(45) DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT NULL,
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(45) DEFAULT NULL,
  `last_sign_in_ip` varchar(45) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `bio` text,
  `receive_announcements` int(11) DEFAULT NULL,
  `avatar_image_uid` varchar(100) DEFAULT NULL,
  `authentication_token` varchar(100) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `IDX_auth_token` (`authentication_token`),
  UNIQUE KEY `IDX_reset_password` (`reset_password_token`)
)
```